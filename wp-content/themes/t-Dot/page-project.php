<?php 
/**
 * Template Name: Project Page
 */
get_header(); ?>
<div class="projectHeader">
</div>
<div id="barba-wrapper">
    <div class="barba-container" data-namespace="projectpage">
        <main id="page">
            <?php 
                $logo = get_theme_mod( 'themeslug_logo' );
                if($logo != ""){
                    $link = get_bloginfo('url');
                    $homeLink ="<a id='logo-container' href='$link' class='logoLink'><img src='$logo'></a>";
            
                } else {
                    $link = get_bloginfo('url');
                    $siteName = get_bloginfo('name');
                $homeLink ="<a id='logo-container' href='$link' class='homeLink'>$siteName</a>";
                }
            ?>
            <?php 
            if(!is_front_page()){
            }?>
            <section class="project-section">
                <div class="container" style="position:relative;"> 
                    <?php  
                        $counter = 1;
                        $args = array(
                            'post_type' => 'project',
                            'posts_per_page' => '-1',
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                        );
                        $the_query = new WP_Query( $args );
                            // The Loop
                        if ( $the_query->have_posts() ) :
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                            $image = types_render_field('project-image',array('raw'=>'true'));
                            $imageMobile = types_render_field('project-image-mobile',array('raw'=>'true'));
                            $title = get_the_title();
                            $reverse = "";
                            if($counter % 2){
                                $reverse = "reverse";
                            }else{
                                $reverse = "";
                            }
                            ?>
                            <a class="project-grid" href="<?php echo get_the_permalink();?>">
                                <div class="project-feature load-hidden <?php echo $reverse?>">
                                    <div class="image desktopProject" data-image="<?php echo $image;?>" style="background-image:url(<?php echo $image ?>)">
                                        <div class="overlay"></div>
                                    </div>
                                    <div class="image mobileProject" data-image="<?php echo $imageMobile;?>" style="background-image:url(<?php echo $imageMobile ?>)">
                                        <div class="overlay"></div>
                                    </div>
                                    <h2><?php echo get_the_title(); ?></h2> 
                                </div>
                            </a>
                    <?php
                        // Do Stuff
                        $counter++;
                        endwhile;
                        endif;
                        // Reset Post Data
                        wp_reset_postdata();
                    ?>
                </div>
            </section>
<?php get_footer(); ?>