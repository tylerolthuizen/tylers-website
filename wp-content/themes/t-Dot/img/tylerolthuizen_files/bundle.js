/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _fadetrans = __webpack_require__(1);

var _fadetrans2 = _interopRequireDefault(_fadetrans);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// jquery compatibility mode , $ is not defined fix

(function ($) {

	/*------------------------------------------------------------------
 
 	Document Ready Function
 
 ---------------------------------------------------------------------
 */

	$(document).ready(function () {
		var navHeight = $('nav').outerHeight(true);
		TweenLite.set($('li .image1 .image-wrap'), { autoAlpha: 0 });
		TweenLite.set($('.active .image1 .image-wrap'), { autoAlpha: 0 });
		TweenLite.set($('.main-nav'), { y: '-=20' });
		TweenLite.set($('.main-nav a'), { autoAlpha: 0 });
		TweenLite.set($('.contact-call'), { autoAlpha: 0 });
		$('body').css('padding-top', navHeight);
		var typed = new Typed('#typed', {
			showCursor: false,
			stringsElement: '#typed-strings',
			typeSpeed: 15,
			onComplete: function onComplete(self) {
				showImages();revealNav();
			}

		});

		function showImages() {
			revealImage();
		}
		function revealNav() {
			TweenLite.to($('.main-nav'), .8, { y: 0,
				ease: Power1.easeIn
			});
			TweenLite.to($('.main-nav a'), .8, { autoAlpha: 0.8,
				ease: Power1.easeIn
			});
		}
		function revealImage() {
			TweenMax.to($('.active .image1 .blue-box'), 0.9, {
				width: '100%',
				ease: Power1.easeIn,
				onComplete: fadeBlock
			});
		}

		function fadeBlock() {
			TweenLite.set($('.active .image1 .image-wrap'), { autoAlpha: 1 });
			TweenLite.to($('.active .image1 .blue-box'), 0.5, { autoAlpha: 0, ease: Power3.ease, onComplete: showContact });
		}
		function showContact() {
			TweenMax.to($('.contact-call'), 0.8, {
				autoAlpha: 1,
				ease: Power1.easeInOut
			});

			TweenLite.set($('.active .image1 .blue-box'), { width: '0%' });
			$('.contact-call a').addClass('fadedIn');
			$('.active .image-wrap h2').addClass('active');
			console.log('calling SlideMe');
			draggableWidth();
			startSlideInterval();
		}

		Barba.Pjax.start();
		/**
   * Next step, you have to tell Barba to use the new Transition
   */

		Barba.Pjax.getTransition = function () {
			/**
    * Here you can use your own logic!
    * For example you can use different Transition based on the current page or link...
    */

			return _fadetrans2.default;
		};

		function slideMe(active) {
			var nextChild = active;
			var activeProject = $('.images .active');
			var next;
			if (nextChild != undefined) {
				next = $(".images li:nth-child(" + nextChild + ")");
				console.log('nextChild' + nextChild);
			} else {
				next = activeProject.next();
				if (next.length < 1) {
					console.log('hello');
					next = $('.images li:first-child');
				}
			}
			if (nextChild == activeProject.index() + 1) {} else {
				var leftVal = $('.draggable').width() * next.index();
				TweenLite.to($(".draggable"), 0.8, { left: leftVal });
				TweenLite.set(next, { autoAlpha: 1 });
				var nextBlue = next.find('.image1 .blue-box');
				TweenLite.set(nextBlue, { width: '100%', autoAlpha: 0 });
				activeProject.find('.image-wrap h2').removeClass('active');
				TweenLite.set($('.active .image1 .blue-box'), { autoAlpha: 1 });
				TweenMax.to($('.active .image1 .blue-box'), 1, {
					width: '100%',
					ease: Power4.easeInOut,
					onComplete: function onComplete() {
						activeProject.removeClass('active');
						TweenLite.set(activeProject, { autoAlpha: 0 });
						TweenLite.set(nextBlue, { autoAlpha: 1 });
						next.addClass('active');
						TweenLite.set($('.active .image1 .image-wrap'), { autoAlpha: 1 });
						TweenLite.to($('.active .image1 .blue-box'), .7, { width: '0%', ease: Power4.easeInOut, onUpdate: function onUpdate() {
								$('.active .image-wrap h2').addClass('active');
							} });
						nextChild = undefined;
					}
				});
			}

			clearInterval(projectSlide);
			startSlideInterval();
		}

		var projectSlide;
		function startSlideInterval() {
			projectSlide = setInterval(function () {
				slideMe();
			}, 5500);
		}

		function draggableWidth() {
			var length = $('.images li').length;
			var count = 1;
			// for (var i = 0; i < length; i++) {
			// 	var left = (100 / length) * count;
			// 	$('.line-drag-bar').append('<span class="measure" style="left:'+left+'%;" data-child="'+count+'"></span>');
			// 	count ++;
			// }
			var width = 100 / length;
			var parentWidth = $('.myIntro .line-drag-bar').width();
			$('.draggable').width(width + '%');
			$(".draggable").draggable({
				axis: "x",
				containment: "parent",
				start: function start(event, ui) {
					clearInterval(projectSlide);
				},
				stop: function stop(event, ui) {
					clearInterval(projectSlide);
					var draggableVal = $(".draggable").position().left + $('.draggable').width();
					var child = Math.round(draggableVal / parentWidth * 100) / 100 * length;
					var child = Math.round(child);

					var leftVal = $('.draggable').width() * child - $('.draggable').width();
					if (child == 1) {
						leftVal = 0;
					}
					console.log('child ' + child);
					console.log('left ' + leftVal);
					TweenLite.to($(".draggable"), 0.8, { left: leftVal, onComplete: function onComplete() {} });
					slideMe(child);
				}

			});
		}

		$('header').css('margin-top', $('.main-nav').height());

		/*-----------------------------------
  
  	Custom Function Calls
  
  ------------------------------------*/

		isoGo();
		navActivator();
		subToggle();
		searchActivator();
		imageStretch();
		// Ajax_Loader();
		// imageStretch();
		menuListPoisition();

		/*-----------------------------------
  		Other Scripts 
  	------------------------------------*/

		$(".menu-item-has-children").hover(function () {
			if ($(window).width() >= 992) {
				var windowWidth = $(window).outerWidth();
				var leftPos = $(this).find('.sub-menu-wrap').find('.sub-menu').offset().left;
				var ulWidth = $(this).find('.sub-menu-wrap').find('ul').outerWidth();
				console.log(windowWidth);
				console.log(leftPos);
				console.log(ulWidth);
				if (leftPos + ulWidth > windowWidth) {
					$(this).find('.sub-menu-wrap').addClass('left-menu-align');
				}
				var subHeight = $(this).find('.sub-menu').outerHeight(true);
				$(this).children('.sub-menu-wrap:first').stop().animate({
					height: subHeight
				}, 150, function () {
					$(this).css({ 'overflow': 'visible', 'height': 'auto' });
				});
			}
		}, function () {
			$('.sub-menu-wrap').removeAttr('style');
		});

		$('.image-wrap').click(function () {
			var image = $(this).attr('data-image');
			var offsetTop = $(this).offset().top;
			var offsetLeft = $(this).offset().left;
			var width = $(this).width();
			var height = $(this).height();
			$('.projectHeader').css({ top: offsetTop, left: offsetLeft, height: height, width: width });
			$('.projectHeader').append('<img src="' + image + '">');
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			$('#barba-wrapper').fadeOut('500', function () {

				TweenLite.to($('.projectHeader'), 1, { width: '100%', delay: .5, left: 0, top: navHeight });
			});
		});
	}); //End of Document Ready


	/*------------------------------------------------------------------
 
 	Window On Resize Function
 
 ---------------------------------------------------------------------
 */

	$(window).resize(function () {
		isoGo();
		menuListPoisition();
		// imageStretch();
	});

	/*------------------------------------------------------------------
 
 	Window On Scroll Function
 
 ---------------------------------------------------------------------
 */

	$(window).scroll(function () {
		scrolldeely();
		// imageStretch();
		var widthCalc = ($(window).width() - $('.container').width()) / 2.5;
		if ($(window).scrollTop() > 20) {
			TweenMax.to($('.right-nav'), .5, {
				x: widthCalc
			});
		} else {
			TweenMax.to($('.right-nav'), .5, {
				x: 0
			});
		}
	});

	/*-------------------------------------------------
 
 	Custom Functions
 
 --------------------------------------------------
 */

	function isoGo() {
		var $grid = $('.port-grid').isotope({
			itemSelector: '.port-item',
			stagger: 60
		});

		$('.filter-button-group').on('click', 'button', function () {
			$('.filter-button-group').find('.active').removeClass('active');
			$(this).addClass('active');
			var filterValue = $(this).attr('data-filter');
			$('.port-grid').isotope({ filter: filterValue });
		});

		$('.filter').on('click', function () {
			var filterValue = $(this).attr('data-filter');
			$('.port-grid').isotope({ filter: filterValue });
			console.log(filterValue);
			return false;
		});
	}

	function Ajax_Loader() {

		// ajaxLoad options = Parent, Load More Class , Load More Anchor Tag, Where you are putting the new Content, Feature Class, Post Type
		$(document).on('click', '.load a', function (event) {
			event.preventDefault();
			ajaxLoad('#pageContent', '.load', '.load a', '.new-content', 'news', 'test-type');
		});
		// Offset must be outside because it will always be changing 
		// To Change how to output looks , go to the my_ajax_pagination() function in the functions.php file.
		var offset = 1;
		ajaxLoad();
		function ajaxLoad(parent, loaderClass, click, newcontent, featureName, postType) {
			var loader = '<div class="loader"> </div>';
			var more = "";
			var parentClass = parent;
			var contentArea = newcontent;
			var offsetCheck = $("" + parentClass + " ." + featureName + " ").length;

			console.log(offset, offsetCheck);
			if (offset > offsetCheck) {
				console.log('greater');
				offset = offsetCheck;
			} else if (offset < offsetCheck) {
				offset = offsetCheck + 1;
			}

			$.ajax({
				url: ajaxpagination.ajaxurl,
				type: 'post',
				data: {
					action: 'ajax_pagination',
					offset: offset,
					featurename: featureName,
					postType: postType
				},
				beforeSend: function beforeSend() {
					$(click).remove();
					$(loaderClass).hide().append(loader).fadeIn('slow');
				},
				success: function success(html) {
					var newItem = html;
					console.log(newItem);
					if (newItem == "<p> No More Posts </p>") {

						$(loaderClass).find('.loader').remove();
						$(loaderClass).hide().prepend(html).fadeIn('fast');
					} else {
						$(newItem).hide().appendTo("" + newcontent + "").fadeIn('slow');
						$(loaderClass).find('.loader').remove();
						$(loaderClass).hide().prepend('<a href="#">More Posts</a>').fadeIn('slow');
					}
					// $(new_content).hide().append(html).fadeIn('slow');
				}
			});
			offset = offset + 1;
		}
	}

	function scrolldeely() {
		var headerHeight = $('#navHead').outerHeight();
		var scrolled = $(document).scrollTop();
		if (scrolled > headerHeight) {
			$('#navHead').addClass('off-canvas');
		} else {
			$('#navHead').removeClass('off-canvas');
		}

		if (scrolled > scroll) {
			$('#navHead').removeClass('fixed');
		} else {
			$('#navHead').addClass('fixed');
		}
		scroll = $(document).scrollTop();
	}

	function navActivator() {
		$('.nav-actiavator').click(function () {
			if (!$(this).hasClass("open")) {
				var navHeight = $('.mobileWrap').find('ul').outerHeight(true);

				$('.mobileWrap').stop().animate({
					height: navHeight
				}, 150, function () {
					$(this).css({ 'overflow': 'scroll', 'height': 'auto', 'max-height': $(window).height() - 56 });
				});
				$(this).addClass('open');
				$('body').css('overflow', 'hidden');
			} else {
				$('.sub-menu-wrap').stop().animate({
					height: 0
				}, 150, function () {
					$(this).removeAttr('style');
				});

				//resetting the toggle function, will reset the toggle so it can be opened again when the main menu is closed
				$('.sub-toggle').unbind('click').toggle(function () {
					var $sub = $(this).siblings('.sub-menu-wrap');
					var subHeight = $sub.children('ul').outerHeight(true);
					$sub.stop().animate({
						height: subHeight
					}, 150, function () {
						$(this).css('height', 'auto');
					});
				}, function () {
					var $sub = $(this).siblings('.sub-menu-wrap');
					$sub.stop().animate({
						height: 0
					}, 150);
				});

				$('.mobileWrap').stop().animate({
					height: 0
				}, 150, function () {
					$(this).removeAttr('style');
				});
				$(this).removeClass("open");
				$('body').css('overflow', 'auto');
			}
		});
	}

	function subToggle() {
		$('.sub-toggle').each(function () {
			$(this).append(' <i class="material-icons">keyboard_arrow_down</i>');
		});

		$('.sub-menu-wrap').each(function () {
			$(this).prepend("<div class=\"arrow-up\"> </div> ");
		});

		$('.sub-toggle').unbind('click').toggle(function () {
			var $sub = $(this).siblings('.sub-menu-wrap');
			var subHeight = $sub.children('ul').outerHeight(true);
			$sub.stop().animate({
				height: subHeight
			}, 150, function () {
				$(this).css('height', 'auto');
			});
		}, function () {
			var $sub = $(this).siblings('.sub-menu-wrap');
			$sub.stop().animate({
				height: 0
			}, 150);
		});

		//initial toggle function, will run when the menu is first opened
		$('.sub-toggle').toggle(function () {
			var $sub = $(this).siblings('.sub-menu-wrap');
			var subHeight = $sub.children('ul').outerHeight(true);
			$sub.stop().animate({
				height: subHeight
			}, 150, function () {
				$(this).css('height', 'auto');
			});
		}, function () {
			var $sub = $(this).siblings('.sub-menu-wrap');
			$sub.stop().animate({
				height: 0
			}, 150);
		});
	}

	function searchActivator() {
		$('.search-act').click(function () {
			$('.search-open').toggle();
			$('.search-close').toggle();
			$('.searchWrap').slideToggle(150);
			if ($('.searchWrap').is(":visible")) {
				$('.searchWrap').find('input').focus();
			}
		});
	}

	function imageStretch() {
		var windowWidth = $(window).width();
		var container = $('.wrap').outerWidth();
		var marginValue = windowWidth - container;
		console.log(marginValue);
		if (windowWidth >= 768) {
			$('.image-left').css('margin-left', -marginValue / 2);
			$('.image-left').css('margin-right', 0);
			$('.image-right').css('margin-left', 0);
			$('.image-right').css('margin-right', -marginValue / 2);
		} else {
			$('.image-left').css('margin-left', -marginValue / 2);
			$('.image-left').css('margin-right', -marginValue / 2);
			$('.image-right').css('margin-left', -marginValue / 2);
			$('.image-right').css('margin-right', -marginValue / 2);
		}
	}

	function menuListPoisition() {}

	var forEach = function forEach(t, o, r) {
		if ("[object Object]" === Object.prototype.toString.call(t)) for (var c in t) {
			Object.prototype.hasOwnProperty.call(t, c) && o.call(r, t[c], c, t);
		} else for (var e = 0, l = t.length; l > e; e++) {
			o.call(r, t[e], e, t);
		}
	};

	var hamburgers = document.querySelectorAll(".hamburger");
	if (hamburgers.length > 0) {
		forEach(hamburgers, function (hamburger) {
			hamburger.addEventListener("click", function () {
				this.classList.toggle("is-active");
			}, false);
		});
	}

	//BarBa REload Scripts

})(jQuery); //Imports

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var FadeTransition = Barba.BaseTransition.extend({
	start: function start() {
		/**
   * This function is automatically called as soon the Transition starts
   * this.newContainerLoading is a Promise for the loading of the new container
   * (Barba.js also comes with an handy Promise polyfill!)
   */

		// As soon the loading is finished and the old page is faded out, let's fade the new page
		var widthCalc = ($(window).width() - $('.container').width()) / 2.5;

		$('#hamburger-icon').removeClass('active');
		TweenMax.to($navSlide, .5, {
			x: '100%',
			ease: Back.Power1
		});

		if ($(window).scrollTop() > 20) {
			TweenMax.to($('.right-nav'), .5, {
				x: widthCalc
			});
		} else {
			TweenMax.to($('.right-nav'), .5, {
				x: 0
			});
		}
		$('.overlay').fadeOut(400);

		Promise.all([this.newContainerLoading, this.fadeOut()]).then(this.fadeIn.bind(this));
	},

	fadeOut: function fadeOut() {
		/**
   * this.oldContainer is the HTMLElement of the old Container
   */

		return $(this.oldContainer).animate({ opacity: 0 }).promise();
	},

	fadeIn: function fadeIn() {
		/**
   * this.newContainer is the HTMLElement of the new Container
   * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
   * Please note, newContainer is available just after newContainerLoading is resolved!
   */

		var _this = this;
		var $el = $(this.newContainer);

		$(this.oldContainer).hide();

		$el.css({
			visibility: 'visible',
			opacity: 0
		});

		$('header').css('margin-top', $('.main-nav').height());

		$el.animate({ opacity: 1 }, 400, function () {
			/**
    * Do not forget to call .done() as soon your transition is finished!
    * .done() will automatically remove from the DOM the old Container
    */

			_this.done();
		});

		var $headerSlide = $('header');

		TweenMax.fromTo($headerSlide, 1, { y: '30%',
			autoAlpha: 0 }, { y: '0%',
			autoAlpha: 1,
			delay: .5
		});

		// CONTENT

		TweenMax.fromTo($('.section'), 1.5, { autoAlpha: 0 }, {
			autoAlpha: 1,
			ease: Power0.ease,
			delay: 0.8
		});
	}
});

exports.default = FadeTransition;

/***/ })
/******/ ]);