var $navSlide = $('.nav-slide');

TweenMax.to($navSlide, 0,{
	x:'100%',

});

TweenLite.set(".nav-slide", {visibility:"visible"});
TweenLite.set("body", {visibility:"visible"});

$('#hamburger-icon').click(function(e){
	e.preventDefault();
	 var widthCalc = ($(window).width() - $('.container').width()) / 2.5;
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			TweenMax.to($navSlide, .5,{
				x:'100%',
				ease:Back.Power1,
			});

			if($(window).scrollTop() > 20){
			 	TweenMax.to($('.right-nav'), .5,{
					x:widthCalc,
				});
			 }else{
			 	TweenMax.to($('.right-nav'), .5,{
					x:0,
				});
			 }
			$('.overlay').fadeOut(400);
		}else{
			$('.overlay').fadeIn(400);
			$(this).addClass('active');
			TweenMax.to($navSlide, .5,{
				x:'0%',
				ease:Power1.easeOut,
			});
			TweenMax.to($('.right-nav'), .5,{
				x:widthCalc,
			});
		}
});



// HEADER

var $headerSlide = $('header');

TweenMax.fromTo($headerSlide, 1,
	{y:'30%',
	autoAlpha:0},
	{y:'0%',
	autoAlpha:1,
	delay:.5,
	}
);


// CONTENT

TweenMax.fromTo($('.section'), 1.3,
	{autoAlpha:0,},
	{
	autoAlpha:1,
	ease:Power0.easeOut,
	delay: 0.8,
	}
);