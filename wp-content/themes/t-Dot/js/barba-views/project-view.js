export {Projectpage};
var Projectpage = Barba.BaseView.extend({
    namespace: 'projectpage',
    onEnter: function() {
        const mainNav= document.querySelector('.main-nav');
        const projectHeader = document.querySelector('.projectHeader');
        var attribute = projectHeader.getAttribute('style');
        
		if(attribute){
			projectHeader.removeAttribute('style');
		}
        if(mainNav.classList.contains('active')){

		}else{
			mainNav.classList.add('active');
			TweenLite.set($('.main-nav'),{y:'-=20'});
			TweenLite.set($('.main-nav a'),{autoAlpha:0});
			TweenLite.set($('.main-nav .hamburger'),{autoAlpha:0});
			TweenLite.set($('.contact-call'),{autoAlpha:0});
        }
       
        var $div2 = $('.project-feature h2');
        $div2.each(function(){
          var h2 = $(this);
          var divWords = $(this).text().split(/\s+/);
          $(this).empty();
          $.each(divWords, function(i,v){
            h2.append("<span class='span-holder'>" + v + "</span>");
              
          });
        });

        var $div = $('.project-feature h2 .span-holder');
        $div.each(function(){
          var h2 = $(this);
          var divWords = $(this).text().split("");
          $(this).empty();
          $.each(divWords, function(i,v){
              if(v == ""){
                h2.append("<br>");
              }else{
                  h2.append("<span class='outerspan'><span class='inner-span'>" + v + "</span></span>");
              }
              
          });
        });
    },
    onEnterCompleted: function() {
        revealNav();
        function revealNav(){
            TweenLite.set($('.main-nav'),{autoAlpha:1});
            TweenLite.to($('.main-nav'),.8,
                {y:0,
                    ease:Power1.easeIn,
                }
            );
            TweenLite.to($('.main-nav a'),.8,
                {autoAlpha:0.8,
                    ease:Power1.easeIn,
                }
            );
            TweenLite.to($('.main-nav .hamburger'),.8,
                {autoAlpha:0.8,
                    ease:Power1.easeIn,
                }
            );
        }
        
        TweenMax.to($('.contact-call'), 0.8,{
			autoAlpha:1,
            ease:Power1.easeInOut,	
            onComplete:function(){
                $('.contact-call a').addClass('fadedIn');
            }
          });
          
          var marginOffset = $('.project-feature').css('margin-top');
          marginOffset =  parseInt(marginOffset,10);
          ScrollReveal().reveal('.project-feature', {
            reset: true, 
            viewFactor: 0.4,
            viewOffset:{
                top:marginOffset
            },
            afterReveal: function(el){
                var elSpan = $(el).find('h2 .inner-span');
                TweenMax.to($(elSpan),.5,{x:'0%'});
            },
            afterReset: function(el){
                var elSpan = $(el).find('h2 .inner-span');
                TweenMax.to($(elSpan),.5,{x:'-100%'});
            }
        });     
    },
    onLeave: function() {
       

    },
    onLeaveCompleted: function() {
       
    }
  });