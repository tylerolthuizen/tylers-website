import TypeIt from 'typeit';
var instance;
var projectSlide; 
export {Homepage};
var Homepage = Barba.BaseView.extend({
    namespace: 'homepage',
    onEnter: function() {
		// The new Container is ready and attached to the DOM.
		clearInterval(projectSlide);
		const projectHeader = document.querySelector('.projectHeader');
		const imageWrap = document.querySelectorAll('li .image1 .image-wrap');
		const imageWrapActive = document.querySelector('li.active .image1 .image-wrap');
		if(projectHeader.getAttribute('style')){
			projectHeader.removeAttribute('style');
		}
		

		document.getElementById('typedString').style.opacity="0";
		const mainNav= document.querySelector('.main-nav');
		if(mainNav.classList.contains('active')){

		}else{
			mainNav.classList.add('active');
			TweenLite.set($('.main-nav'),{y:'-=20'});
			TweenLite.set($('.main-nav a'),{autoAlpha:0});
			TweenLite.set($('.main-nav .hamburger'),{autoAlpha:0});
			TweenLite.set($('.contact-call'),{autoAlpha:0});
		}
		
		
    },
    onEnterCompleted: function() {
	TweenMax.set($('.projectHeader'),{
		height:0,
	});
	document.getElementById('typedString').style.opacity="1";
	const fxWord = document.querySelectorAll('.fx .word');
	fxWord.forEach( el => {
		el.classList.add('slide');
	});
	instance = new TypeIt('#typedString', {
		speed:20,
		afterComplete: function (instance) {
			showImages();
			revealNav();
		}
	})
	.destroy();

  	function showImages(){
		TweenLite.set($('.active .image1 .image-wrap'),{autoAlpha:1});
		TweenMax.set($('.active .image1 .blue-box'),{width:'100%'});
		revealImage();
  	}
  	function revealNav(){
  		TweenLite.to($('.main-nav'),.8,
  			{y:0,
  				ease:Power1.easeIn,
  			}
  		);
  		TweenLite.to($('.main-nav a'),.8,
  			{autoAlpha:0.8,
  				ease:Power1.easeIn,
  			}
		  );
		  TweenLite.to($('.main-nav .hamburger'),.8,
  			{autoAlpha:0.8,
  				ease:Power1.easeIn,
  			}
  		);
	  }

	function revealImage(){
		
		TweenMax.to($('.active .image1 .blue-box'), 0.9,{
			width:'0%',
			ease:Power1.easeIn,
			onComplete:showContact,	
		  });	
		  TweenLite.set($('.main-nav'),{autoAlpha:1});
		  
	}

	
	function showContact(){
		TweenLite.to($('.active .image1 .blue-box'), 0.5,{autoAlpha:0,ease:Power3.ease,});
		TweenMax.to($('.contact-call'), 0.8,{
			autoAlpha:1,
			ease:Power1.easeInOut,	
		  });
		TweenMax.to($('.more-projects'),.8,{
			autoAlpha:1,
		})

  		TweenLite.set($('.active .image1 .blue-box'), {width:'0%'});
		$('.contact-call a').addClass('fadedIn');
		$('.active .image-wrap h2').addClass('active');
		draggableWidth();
		startSlideInterval();
		
	}

	function slideMe(active){
		var nextChild = active;
		var activeProject = $('.images .active');
		var next;
		if(nextChild != undefined){
			next = $(".images li:nth-child("+nextChild+")");
		}else{
			 next = activeProject.next();
			if(next.length < 1){
				next = $('.images li:first-child');

			}
		}
		if(nextChild == activeProject.index() + 1 ){
			
		}else{
			var leftVal = ($('.draggable').width() * next.index());
			TweenLite.to($(".draggable"),0.8,{left:leftVal,delay:.2,});
			TweenLite.set(next,{autoAlpha:1,});
			var nextBlue = next.find('.image1 .blue-box');
			TweenLite.set(nextBlue,{width:'100%',autoAlpha:0,});
			activeProject.find('.image-wrap h2').removeClass('active');
			TweenLite.set($('.active .image1 .blue-box'),{autoAlpha:1,});
			TweenMax.to($('.active .image1 .blue-box'), 1,{
				width:'100%',
				ease:Power4.easeInOut,
				onComplete:function(){
					activeProject.removeClass('active');
					TweenLite.set(activeProject,{autoAlpha:0,});
					TweenLite.set(nextBlue,{autoAlpha:1,});
					next.addClass('active');
					TweenLite.set($('.active .image1 .image-wrap'),{autoAlpha:1});
					TweenLite.to($('.active .image1 .blue-box'), .7,{width:'0%',ease:Power4.easeInOut,onUpdate:function(){$('.active .image-wrap h2').addClass('active');}});
					nextChild = undefined;
					
				},	
	  		});	
		}
  		clearInterval(projectSlide);
  		startSlideInterval();
	}
	
	function startSlideInterval(){
		projectSlide = setInterval(function(){
			slideMe();
		},4500);
	}

	$('.images li').click(function(){
		clearInterval(projectSlide);
	});
	
	function draggableWidth(){
		var length = $('.images li').length;
		var count = 1;
		// for (var i = 0; i < length; i++) {
		// 	var left = (100 / length) * count;
		// 	$('.line-drag-bar').append('<span class="measure" style="left:'+left+'%;" data-child="'+count+'"></span>');
		// 	count ++;
		// }
		var width = 100 / length;
		var parentWidth = $('.myIntro .line-drag-bar').width();
		$('.draggable').width(width+'%');
		$(".draggable").draggable({
		  axis: "x",
		  containment: "parent",
			start: function(event, ui){
				clearInterval(projectSlide);	
			},
	  		stop: function( event, ui ) {
	  			clearInterval(projectSlide);
	  			var draggableVal = $(".draggable").position().left + $('.draggable').width();
	  			var child = (Math.round((draggableVal/parentWidth)*100) / 100 )*length;
	  			var child = Math.round(child);

	  			var leftVal = ($('.draggable').width() * child) - $('.draggable').width();
	  			if(child == 1){
	  				leftVal = 0;
	  			}

					TweenLite.to($(".draggable"),0.2,{left:leftVal,ease: Power1.easeInOut,onComplete:function(){
					
	  				}});
				
	  			
	 			slideMe(child);
		  	}
		});
	}

    },
    onLeave: function(projectSlide,instance) {
		// A new Transition toward a new page has just started.
		clearInterval(projectSlide);
		const fxWord = document.querySelectorAll('.fx .word');
		fxWord.forEach( el => {
			el.classList.remove('slide');
		});
    },
    onLeaveCompleted: function() {
		// The Container has just been removed from the DOM.
		clearInterval(projectSlide);
    }
});
