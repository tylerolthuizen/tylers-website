
export {aboutPage};
var aboutPage = Barba.BaseView.extend({
    namespace: 'aboutpage',
    onEnter: function() {
        // The new Container is ready and attached to the DOM.

        TweenMax.set($('.main-nav'),{
            autoAlpha:0,
        });
        $('.main-nav').removeClass('active');
       
    },
    onEnterCompleted: function() {
        // The Transition has just finished.
        const projectHeader = document.querySelector('.projectHeader');
        var attribute = projectHeader.getAttribute('style');
        
		if(attribute){
			projectHeader.removeAttribute('style');
        }

        var oldPage = Barba.HistoryManager.prevStatus();
        if(oldPage){
            $('.return a').attr('href',''+oldPage.url+'');
        }

        $('.about-content').addClass('active');
        $('.return').addClass('active');
        $('.follow-contact').addClass('active');
    },
    onLeave: function() {
        // A new Transition toward a new page has just started.

    },
    onLeaveCompleted: function() {
        // The Container has just been removed from the DOM.
    }
  });