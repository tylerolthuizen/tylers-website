export {singleProject};
var singleProject = Barba.BaseView.extend({
    namespace: 'singleproject',
    onEnter: function() {
        // The new Container is ready and attached to the DOM.
        const mainNav= document.querySelector('.main-nav');
        if(mainNav.classList.contains('active')){

		}else{
			mainNav.classList.add('active');
			TweenLite.set($('.main-nav'),{y:'-=20'});
			TweenLite.set($('.main-nav a'),{autoAlpha:0});
			TweenLite.set($('.main-nav .hamburger'),{autoAlpha:0});
			TweenLite.set($('.contact-call'),{autoAlpha:0});
        }

        var $div2 = $('.project-title h1');
        $div2.each(function(){
          var h2 = $(this);
          var divWords = $(this).text().split(/\s+/);
          $(this).empty();
          $.each(divWords, function(i,v){
            h2.append("<span class='span-holder'>" + v + "</span>");
              
          });
        });

        var $div = $('.project-title h1 .span-holder');
        $div.each(function(){
          var h2 = $(this);
          var divWords = $(this).text().split("");
          $(this).empty();
          $.each(divWords, function(i,v){
              if(v == ""){
                h2.append("<br>");
              }else{
                  h2.append("<span class='outerspan'><span class='inner-span'>" + v + "</span></span>");
              }
              
          });
        });

       
        
    },
    onEnterCompleted: function() {
        // The Transition has just finished.
        $('.project-title h1 .span-holder').each(function(){
            if($(this).width() > ($(this).parent().width() *.8) ){
                $(this).css('display','block');
                var reduceHeight = $('.project-title h1 .span-holder').height() * .3;
                $('.project-info').css('margin-top','-'+reduceHeight+'px');
           
            }
        });
        TweenMax.to($('.project-title h1 .inner-span'),.5,{x:'0%'});
        revealNav();
        function revealNav(){
            TweenLite.set($('.main-nav'),{autoAlpha:1});
            TweenLite.to($('.main-nav'),.8,
                {y:0,
                    ease:Power1.easeIn,
                }
            );
            TweenLite.to($('.main-nav a'),.8,
                {autoAlpha:0.8,
                    ease:Power1.easeIn,
                }
            );
            TweenLite.to($('.main-nav .hamburger'),.8,
                {autoAlpha:0.8,
                    ease:Power1.easeIn,
                }
            );
        }
        TweenMax.to($('.contact-call'), 0.8,{
			autoAlpha:1,
            ease:Power1.easeInOut,	
            onComplete:function(){
                $('.contact-call a').addClass('fadedIn');
            }
        });

        ScrollReveal().reveal('.reveal-image',{viewFactor:0.4,});
        const projectHeader = document.querySelector('.projectHeader');
        var attribute = projectHeader.getAttribute('style');
        
		if(attribute){
			projectHeader.removeAttribute('style');
        }

    },
    onLeave: function() {
        // A new Transition toward a new page has just started.
    },
    onLeaveCompleted: function() {
        // The Container has just been removed from the DOM.
    }
  });