import { TweenLite } from "gsap";

export {aboutTransition};
var aboutTransition = Barba.BaseTransition.extend({
    start: function() {
      /**
       * This function is automatically called as soon the Transition starts
       * this.newContainerLoading is a Promise for the loading of the new container
       * (Barba.js also comes with an handy Promise polyfill!)
       */

      const projectHeader = document.querySelector('.projectHeader');
      var attribute = projectHeader.getAttribute('style');
      
      if(attribute){
          projectHeader.removeAttribute('style');
      }
  
      // As soon the loading is finished and the old page is faded out, let's fade the new page
        TweenMax.set($('.projectHeader'),{
            zIndex: 999,
            position:'fixed',
            width:'100%',
            height:'100%',
            top:0,
            y:'-100%',
            background:'#11111a',
        });
        Promise
        .all([this.newContainerLoading, this.animateHeader()])
        .then(this.fadeOut.bind(this))
        .then(this.fadeIn.bind(this))
    },
    animateHeader: function(){
        var deferred = Barba.Utils.deferred();
        TweenMax.to($('.projectHeader'),.6,{
            y:'0%',
            delay:.1,
            onComplete:function(){
                deferred.resolve();
            },
        });
        return deferred.promise;
      },
    fadeOut: function() {
        /**
         * this.oldContainer is the HTMLElement of the old Container
         */
        var deferred = Barba.Utils.deferred();
        TweenMax.to($(this.oldContainer),.5,{
          autoAlpha:0,
          onComplete:function(){
            $(this.oldContainer).hide();
              deferred.resolve();
          }
        });
  
        return deferred.promise;
      },

    fadeIn: function() {
      $(window).scrollTop(0); // scroll to top hereb
      /**
       * this.newContainer is the HTMLElement of the new Container
       * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
       * Please note, newContainer is available just after newContainerLoading is resolved!
       */
  
      var _this = this;
      var $el = $(this.newContainer);
  
      
      $el.css({
        visibility : 'visible',
        opacity : 0,
      });
  
      TweenMax.to($el,.2,{
        delay:.2,
        autoAlpha : 1,
        onComplete:function(){
            _this.done();
        },
      });
    }
  });