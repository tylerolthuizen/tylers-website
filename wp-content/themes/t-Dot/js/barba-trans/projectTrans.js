export {projectTransition};
var projectTransition = Barba.BaseTransition.extend({
    start: function() {
      
        Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.fadeIn.bind(this));
    },
  
    fadeOut: function() {

        var deferred = Barba.Utils.deferred();
        if($(window).width() <= 601){
        var elWrap = $('.project.active').find('.image-wrap.mobile-wrap');
        }else{
        var elWrap = $('.project.active').find('.image-wrap.desktop-wrap');
        }
        var url = $(elWrap).attr('data-image');
        var eltop = $(elWrap).offset().top - $(window).scrollTop();
        var elleft = $(elWrap).offset().left;
        var elwidth = $(elWrap).width();
        var elheight = $(elWrap).height();
        
        $('.projectHeader').css({
            'background-image':'url('+url+')',
            'width':elwidth,
            'height':elheight,
            'top': eltop,
            'left':elleft,
            'position': 'fixed',
            'z-index':'-1',
        });

        TweenMax.to($(this.oldContainer),.5,{
            delay:.3,
            autoAlpha:0,
            onComplete:function(){
                TweenMax.to($('.projectHeader'),.6,{
                    top:110,
                    zIndex:1,
                    onComplete:function(){
                    $('.projectHeader').css({
                        'position': 'absolute',
            
                    });
                
                    deferred.resolve();
                    }
                });
                $('.project-header').height(elheight);

            }
        });
        return deferred.promise;
    },
  
    fadeIn: function() {
        
        $(window).scrollTop(0); // scroll to top hereb
   
        var _this = this;
        var $el = $(this.newContainer);

        $(this.oldContainer).hide();

        $el.css({
            visibility : 'visible',
            opacity : 0
        });

        TweenMax.to($el,.5,{
            autoAlpha : 1,
            onComplete:function(){
            TweenMax.set($('.projectHeader'),{
                autoAlpha:0,
                zIndex:-1,
            })
            _this.done();
            },
        });
    }
  });