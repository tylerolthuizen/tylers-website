export {projectGridTransition};
var projectGridTransition = Barba.BaseTransition.extend({
    start: function() {
        $('body').css('overflow','hidden');
      /**
       * This function is automatically called as soon the Transition starts
       * this.newContainerLoading is a Promise for the loading of the new container
       * (Barba.js also comes with an handy Promise polyfill!)
       */
  
      // As soon the loading is finished and the old page is faded out, let's fade the new page
      Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.fadeIn.bind(this));
    },
  
    fadeOut: function() {
      /**
       * this.oldContainer is the HTMLElement of the old Container
       */

      var deferred = Barba.Utils.deferred();
      if($(window).width() <= 601){
        var elWrap = $('.project-grid.active').find('.image.mobileProject');
      }else{
        var elWrap = $('.project-grid.active').find('.image.desktopProject');
      }
      var url = $(elWrap).attr('data-image');
      var eltop = $(elWrap).offset().top - $(window).scrollTop();
      var elleft = $(elWrap).offset().left;
      var elwidth = $(elWrap).width();
      var elheight = $(elWrap).height();
      var animateHeight;
      var animateMin;
      var heightSet;
      if($(window).width() <= 601){
        heightSet = 380;
      }else if($(window).width() > 601 && $(window).width() < 992 ){
        animateMin = 400;
        animateHeight = '60vh';
      }else if($(window).width() >= 992 && $(window).width() <= 1600 ){
        animateHeight = '60vh';
        animateMin = 500;
      }else{
        animateHeight = '60vh';
      }
      if($(window).width() >= 601){
        if($(window).height() * .6 < animateMin ){
         heightSet = animateMin;
        }else{
             heightSet = animateHeight;
        } 
      }
      
      $('.projectHeader').css({
          'background-image':'url('+url+')',
          'width':elwidth,
          'min-height':380,
          'height':elheight,
          'top': eltop,
          'left':elleft,
          'position': 'fixed',
          'z-index':'-1',
      });
      

      var containerWidth = $('.container').width();
      var containerLeft = $('.container').offset().left;
      TweenMax.to($(this.oldContainer),.4,{
          delay:.2,
          autoAlpha:0,
          onComplete:function(){
            TweenMax.to($('.projectHeader'),.5,{
                height: heightSet,
                width: containerWidth,
                left:containerLeft,
                onComplete:function(){
                    TweenMax.to($('.projectHeader'),0.6,{
                        top:110,
                        zIndex:1,
                        onComplete:function(){ 
                          deferred.resolve();
                        }
                    });
               
                    
                }
        
              });
          }
      });

      return deferred.promise;
    },
  
    fadeIn: function() {
      $(window).scrollTop(0); // scroll to top hereb
      /**
       * this.newContainer is the HTMLElement of the new Container
       * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
       * Please note, newContainer is available just after newContainerLoading is resolved!
       */

  
       var _this = this;
       var $el = $(this.newContainer);

       $('.projectHeader').css({
        'position': 'absolute',     
        });
  
       $(this.oldContainer).hide();
  
       $el.css({
         visibility : 'visible',
         opacity : 0
       });

        TweenMax.to($el,.5,{
            delay:.2,
            autoAlpha : 1,
            onComplete:function(){
            TweenMax.set($('.projectHeader'),{
                autoAlpha:0,
                zIndex:-1,
            })
            _this.done();
            },
        });
    }
  });