import { TweenLite } from "gsap";

export {FadeTransition};
var FadeTransition = Barba.BaseTransition.extend({
    start: function() {
      Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.fadeIn.bind(this));
    },
  
    fadeOut: function() {
      var deferred = Barba.Utils.deferred();
      TweenMax.to($(this.oldContainer),.5,{
        autoAlpha:0,
        onComplete:function(){
          deferred.resolve();
        }
      });
      return deferred.promise;
    },
  
    fadeIn: function() {
      $(window).scrollTop(0); // scroll to top hereb  
      var _this = this;
      var $el = $(this.newContainer);
  
      $(this.oldContainer).hide();
  
      $el.css({
        visibility : 'visible',
        opacity : 0,
      });
  
      TweenMax.to($el,.5,{
        delay:.5,
        autoAlpha : 1,
        onComplete:function(){
            _this.done();
        },
      });
    }
  });