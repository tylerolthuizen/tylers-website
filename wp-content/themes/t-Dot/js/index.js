import '../css/main.scss';
require('ev-emitter');

// TweenMax
import {TweenMax} from "gsap";
import ScrollToPlugin from "gsap/ScrollToPlugin";
// Images Loaded
var imagesLoaded = require('imagesloaded');
imagesLoaded.makeJQueryPlugin( $ );
var jQueryBridget = require('jquery-bridget');


// Jquery UI
require('webpack-jquery-ui');
require('webpack-jquery-ui/draggable');
require('jquery-ui-touch-punch');
// BARBA
var Barba = require('barba.js');


// BARBA TRANS

import {FadeTransition} from "./barba-trans/fadeTrans.js";
import {projectTransition} from "./barba-trans/projectTrans.js";
import {projectGridTransition} from "./barba-trans/projectGridTrans.js";
import {aboutTransition} from "./barba-trans/aboutTrans.js";

// BARVA VIEWS


// BARBA VIEWS

import {Homepage} from "./barba-views/home-view.js";
import {Projectpage} from "./barba-views/project-view.js";
import {singleProject} from "./barba-views/single-project-view.js";
import {aboutPage} from "./barba-views/about-view.js";


if ('scrollRestoration' in history) {
	history.scrollRestoration = 'manual';
}


// Don't forget to init the view!
Homepage.init();
Projectpage.init();
singleProject.init();
aboutPage.init();

// START BARBA

Barba.Pjax.start();

Barba.Utils.xhrTimeout = 7000;

var lastClickEl;
Barba.Dispatcher.on('linkClicked', (el) => {
    lastClickEl = el;
});

Barba.Pjax.getTransition = function() {
    /**
     * Here you can use your own logic!
     * For example you can use different Transition based on the current page or link...
     */
    if($(lastClickEl).hasClass('project')){
		
		$(lastClickEl).addClass('active');
		lastClickEl = "";
		return projectTransition;
		
    }else if($(lastClickEl).hasClass('project-grid')){
		
		$(lastClickEl).addClass('active');
		lastClickEl = "";
		return projectGridTransition;
		
	}else if($(lastClickEl).hasClass('aboutLink')){
		
		$(lastClickEl).addClass('active');
		lastClickEl = "";
		return aboutTransition;
		
	}else{
		lastClickEl = "";
		return FadeTransition;
		
    }
    
};

Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;
	Barba.Pjax.preventCheck = function(evt, element) {
  if (!Barba.Pjax.originalPreventCheck(evt, element)) {
    return false;
  }

  // No need to check for element.href -
  // originalPreventCheck does this for us! (and more!)
  if (/.pdf/.test(element.href.toLowerCase())) {
    return false;
  }
  return true;
};


// JQUERY STUFF
(function($) {
	
/*------------------------------------------------------------------

	Document Ready Function

---------------------------------------------------------------------
*/

	$(document).ready(function(){
		
		// ---------------------------------------

			// CSS SETS and Class Sets

		//-----------------------------------------
			
			var navHeight = $('nav').outerHeight(true);
			$('body').css('padding-top',navHeight + 30);
			
			$('.menu li a').each(function(){
				$(this).addClass('stagger-list');
			});
			
			$('header').css('margin-top' , $('.main-nav').height());
			$('.aboutNav a').addClass('aboutLink');
			
		// ---------------------------------------

			// TWEEN SETS

		//-----------------------------------------
		
			$('.stagger-list').each(function(){
				TweenLite.set($(this), {x:40});
			});

			var navHeight = document.querySelector('nav').clientHeight;
			$('.outer-nav-holder').css('height', navHeight);
			TweenMax.set($('body'),{visibility:'hidden'});
			// TweenMax.set($('main .img'),{height:0});
			TweenMax.set($('body'),{visibility:'visible',});
			TweenMax.set($('.nav-mobile'),{'x': '100%'});
			// TweenMax.to($('main .img'),.8,{height:250,ease:Circ.easeInOut, delay:1});


			var $navSlide = $('.nav-slide');
			TweenMax.to($navSlide, 0,{
				x:'100%',

			});
			TweenLite.set(".nav-slide", {visibility:"visible"});

		// -------------------------------------------------
				// TWEEN SETS END 
		//--------------------------------------------------


		// -------------------------------------------------

			// CUSTOM FUNCTIONS

		//-------------------------------------------------
		var wrapperMenu = document.querySelector('.wrapper-menu');
		wrapperMenu.addEventListener('click', function(elem){
			if(this.classList.contains('open')){
				wrapperMenu.classList.remove('open'); 
				$('.menus').removeClass('active');
				TweenMax.to('main',0.6,{autoAlpha:1,delay:.6});
				TweenMax.staggerTo(".stagger-list",0.3, {x:40},0.09);
			}else{
				wrapperMenu.classList.add('open'); 
				TweenMax.to('main', 0.6,
				{autoAlpha:0,
				onComplete:function(){
					$('.menus').addClass('active');
					TweenMax.staggerTo(".stagger-list", 0.5, {x:0, ease:Power1.easeInOut,},0.09);
				}
				});
				
			}
		});

		$('.menu li a').click(function(){
			$('.menus').removeClass('active');
			wrapperMenu.classList.remove('open'); 	
			TweenMax.to('main',0.6,{autoAlpha:1,delay:.4});
		});
		
		
		
			
		
		
		// Call custom functions
	

	}); //End of Document Ready

})( jQuery );