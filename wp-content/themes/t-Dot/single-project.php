<?php get_header(); ?>
<div class="projectHeader"></div>
	<div id="barba-wrapper">
	  <div class="barba-container" data-namespace="singleproject">
			<main id="page">
				<div class="container" style="position:relative;">
					<?php
						if ( have_posts() ) {
							while ( have_posts() ) {
								the_post(); 
								//
								// Post Content here
								$image = types_render_field('project-image', array('raw'=>true));
								$imageMobile = types_render_field('project-image-mobile', array('raw'=>true));
								//
							} // end while
						} // end if
					?>
					<div class="project-header projectDesktop" style="background-image:url(<?php echo $image ?>);background-size: cover;background-position: center center"></div>
					<div class="project-header projectMobile" style="background-image:url(<?php echo $imageMobile ?>);background-size: cover;background-position: center center"></div>
					<div class="project-title">
						<h1><?php echo get_the_title(); ?></h1>
					</div>
				</div>
			<section class="single-project-content">	
				<div class="container">
					<div class="project-info reveal-image load-hidden">
						<div class="row">
							<div class="col-sm-12 col-md-4 col-lg-3 columnDivs">
								<h3>Project Type</h3>
								
								<?php if(types_render_field('agency-name')){ $agencyName = ' - ' . types_render_field('agency-name'); $agencyLink = types_render_field('agency-link');}else{
									$me = "That's Me";
								} ?>
								<p><?php echo types_render_field('project-type') ?> <a class="no-barba" href="<?php echo $agencyLink ?>" target="_blank"> <?php echo $agencyName?></a></p>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 columnDivs">
								<h3>Role</h3>
								<p><?php echo types_render_field('project-role') ?></p>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 columnDivs">
								<h3>Website</h3>
								
								<p><a class="no-barba" target="_blank" href="https://<?php echo types_render_field('website-url',array('raw'=>'true'))?>"><?php echo types_render_field('website-url',array('raw'=>'true'))?></a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="image-wraps">
					<div class="container">
						<div class="project-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>	
				<div class="container">
				<?php 
					// Section 1.
					function filter_next_post_sort( $sort ) {
						$sort = 'ORDER BY p.menu_order ASC LIMIT 1';
						return $sort;
					}

					function filter_next_post_where( $where ) {
						global $post, $wpdb;
						$where = $wpdb->prepare( "WHERE p.menu_order > '%s' AND p.post_type = 'project' AND p.post_status = 'publish'",$post->menu_order );
						return $where;
					}

					function filter_previous_post_sort( $sort ) {
						$sort = 'ORDER BY p.menu_order DESC LIMIT 1';
						return $sort;
					}

					function filter_previous_post_where($where) {
						global $post, $wpdb;

						$where = $wpdb->prepare( "WHERE p.menu_order < '%s' AND p.post_type = 'project' AND p.post_status = 'publish'",$post->menu_order );
						return $where;
					}

					add_filter( 'get_next_post_sort',   'filter_next_post_sort' );
					add_filter( 'get_next_post_where',  'filter_next_post_where' );

					add_filter( 'get_previous_post_sort',  'filter_previous_post_sort' );
					add_filter( 'get_previous_post_where', 'filter_previous_post_where' );

					// Section 2.
					$previous_post = get_previous_post();
					$next_post = get_next_post();

					echo '<div class="adjacent-entry-pagination pagination">';

					if ( $next_post ) {
						echo '<div class="pagination-next"><a href="' .get_permalink( $next_post->ID ). '">Next Project <span>' .$next_post->post_title. '</span></a></div>';
					} else {
						$counter = 1;
						$args = array(
							'post_type' => 'project',
							'posts_per_page' => '1',
							'orderby' => 'menu_order',
							'order' => 'ASC',
						);
						$the_query = new WP_Query( $args );
							// The Loop
							if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
								
							echo '<div class="pagination-next"><a href="' .get_the_permalink(). '">Next Project<span>'.get_the_title().'</span></a></div>';
								
							$counter++;
							endwhile;
							endif;
							// Reset Post Data
							wp_reset_postdata();
					}
					echo '</div>';
				?>
				</div>
			</section>
<?php get_footer(); ?>
