<?php 
/**
 * Template Name: About Page
 */
get_header(); ?>
<div class="projectHeader"></div>
<div id="barba-wrapper">
    <div class="barba-container" data-namespace="aboutpage">
      <main id="page">
        <section class="about-section"> 
            <div class="about-background"></div>
            <div class="container">
              <div class="return">
                  <a href="<?php echo get_home_url()?>" style="color:#fff;"><i class="material-icons">keyboard_arrow_left</i></a>
              </div>  
              <div class="about-content">
                <p>Hello. I'm a full-stack web developer living in Edmonton Alberta. I'm passionate about creating web experiences that are not just easy and enjoyable to use, but that also are visually engaging and unique.
                  Front-end, back-end – I do it all. I love to learn, collaborate and I also make a killer pot of coffee. <i class="fa fa-coffee" aria-hidden="true"></i> <br>
                </p>
                <div class="follow-contact">
                  <div>
                    <h3>Contact Me</h3>
                    <a href="mailto:tylerolthuizen@gmail.com">tylerolthuizen@gmail.com</a>
                  </div>
                  <div>
                    <h3>Follow Me</h3>
                    <ul>
                      <li><a href="https://gitlab.com/tylerolthuizen"><i class="fa fa-gitlab"></i></a></li>
                      <li><a href="https://www.instagram.com/tylerolthuizen/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </section>
<?php get_footer(); ?>