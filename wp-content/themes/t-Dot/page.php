<?php get_header(); ?>
<div id="barba-wrapper">
  	<div class="barba-container">
		<section class="page-content">
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</section>
	</div>
</div>
<?php get_footer(); ?>
