const path = require('path');
var webpack = require('webpack');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
module.exports = {
  entry:{
    bundle: './js/index.js',
    vendor: [
        // local packages
        'jquery',
        // npm packages are added to vendor code separately in splitChunks config below
    ]
  },
  output: {
    filename: '[name].min.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true, // set to true if you want JS source maps,
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
        cacheGroups: {
            commons: {
                test: /[\\/]node_modules[\\/]/,
                name: 'vendor',
                chunks: 'all'
            }
        }
    }
  },
  module:{
    rules:[
        {
            test: /\.js$/,
            exclude:/(node_modules|bower_components)/,
            use:{
                loader: 'babel-loader',
            }
        },
        {
            test: /\.scss$/,
            use: [
                 MiniCssExtractPlugin.loader,
                'css-loader?url=false',
                'resolve-url-loader',
                'sass-loader'  
            ]
        },
        {
            test: /\.css$/,
            loaders: ["style-loader","css-loader"]
        },
        {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
                {
                loader: 'file-loader'
                }
            ]
        }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
        $: 'jquery',
       '$': 'jquery',
       jquery: 'jquery',
       jQuery: 'jquery',  
       "window.jQuery": "jquery'",
      "window.$": "jquery",
       Barba: 'barba.js',
     }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "bundle.css",
      chunkFilename: "[id].css"
    }),
    new OptimizeCSSAssetsPlugin({}),
  ],
  
}