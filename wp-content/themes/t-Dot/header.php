<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<link rel="stylesheet" href="https://use.typekit.net/ofr1tep.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<script src="https://unpkg.com/scrollreveal"></script>
		<script src="https://use.fontawesome.com/b643566424.js"></script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>	
		<nav class="main-nav">
			<div class="container">
				<div class="bar">
					<div class="hamburger">
						<div class="wrapper-menu">
							<div class="line-menu half start"></div>
							<div class="line-menu"></div>
							<div class="line-menu half end"></div>
						</div>
					</div>
					<div class="contact-call">
						<a href="<?php echo home_url() ?>">tyler.olthuizen</a>
					</div>
				</div>	
				<div class="menus">
					<div class="container">
						<?php 
							echo wp_nav_menu();
						?>
						<div class=""><a class="email-float stagger-list no-barba" href="mailto:tylerolthuizen@gmail.com">tylerolthuizen@gmail.com</a></div>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</nav>
