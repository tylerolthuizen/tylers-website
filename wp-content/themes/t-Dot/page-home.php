<?php 
/**
 * Template Name: Home Page
 */
get_header(); ?>

<div class="projectHeader"></div>
<div id="barba-wrapper">
	<div class="barba-container" data-namespace="homepage">
		<main id="page">
			<?php 
			if(!is_front_page()){
			}?>
			<section class="myIntro">
				<div class="container flex">
					<div class="content">
						<h1><span class="fx"><span class="word">Hey</span></span><span class="fx"><span class="word2 word">There</span></span></h1>
						<h4 id="typedString">I'm Tyler , a Full Stack Developer from Edmonton, AB </h4>
					</div>
					<div class="wrap-images">
						<div class="more-projects">
							<a href="/projects"><p>View more projects </p><i class="material-icons">apps</i></a>
						</div>
						<ul class="images">
							<?php
								$args = array(
									'post_type' => 'project',
									'posts_per_page' => '5',
									'orderby' => 'menu_order',
									'order' => 'ASC',
								);
								$the_query = new WP_Query( $args );
								// The Loop
								$counter =1;
								if ( $the_query->have_posts() ) :
								while ( $the_query->have_posts() ) : $the_query->the_post();

								if($counter == 1){
									$active = "active";
								}else{
									$active = "";
								}
								// Do Stuff
								$image = types_render_field('project-image', array('raw'=>true));
								$imageMobile = types_render_field('project-image-mobile', array('raw'=>true));
								$postNumber = '0'.$counter;
							?>
							<li class="<?php echo $active ?>">
								<div class="image1">
									<a class="project" href="<?php echo get_the_permalink();?>">
										<div class="image-wrap desktop-wrap" style="background-image:url(<?php echo $image ?>);background-size: cover;background-position: center center" data-image="<?php echo $image ?>">
											<div class="overlay"></div>						
											<h2><?php echo $postNumber ?></h2>
											<!-- <img  src="<?php echo get_template_directory_uri() ?>/img/you.jpg"> -->
										</div>
										<div class="image-wrap mobile-wrap" style="background-image:url(<?php echo $imageMobile ?>);background-size: cover;background-position: center center" data-image="<?php echo $imageMobile ?>">
											<div class="overlay"></div>						
											<h2><?php echo $postNumber ?></h2>
											<!-- <img  src="<?php echo get_template_directory_uri() ?>/img/you.jpg"> -->
										</div>
										<div class="blue-box"></div>
									</a>
								</div>
							</li>
							<?php	
								$counter ++;
								endwhile;
								endif;
								// Reset Post Data
								wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
				<div class="container">
					<div class="line-drag-bar">
						<div class="draggable"></div>
					</div>
				</div>
			</section>
<?php get_footer(); ?>