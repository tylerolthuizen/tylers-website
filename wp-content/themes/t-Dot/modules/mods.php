<?php 

    class mod{
        var $args;

        function set_args($new_args){
            $this->args = $new_args;
        }
        var $type;
        
        function set_type($new_type){
            $this->type = $new_type; 
            $func= $this->type;
            $this->$func();
        } 

        var $tax;
        function set_tax($new_tax){
            $this->tax = $new_tax; 
        }

        var $param;

        function getParams($params){
            $this->param = $params;
        }

        function news(){
            // print_r($this->args);
            $the_query = new WP_Query($this->args);
            $taxName = $this->tax;
            include 'news-pull/news-filtering.php';
        }

        var $gallSize ;
        function set_gallery_size($new_size){
            $this->gallSize = $new_size;
        }

        function gallery(){
            // print_r($this->args);
            $taxName = $this->tax;
            $galleryCrop = $this->gallSize;
            $the_query = new WP_Query($this->args);
            include 'gallery/gallery.php';    
        }

        function team(){
            $the_query = new WP_Query($this->args);
            include 'team/team.php';    
        }

        function longSlider(){
            
            $the_query = new WP_Query($this->args);
            include 'long-slider/long-slider.php'; 

        }

        function featuredSlider(){
            $the_query = new WP_Query($this->args);
            $titleMain = $this->param;
            include 'featuredSlider/featuredSlider.php'; 
        }
                   
    }

?>