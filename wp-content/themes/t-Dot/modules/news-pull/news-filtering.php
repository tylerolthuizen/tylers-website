<div class="news">
	<div>
		<!--  FILTER CONTROLS -->
		<div class="controls row">
			<div class="filter-button col-s12 col-lg-8">
				<button class="button-style button-8 active" type="button" data-filter="*">All</button>
				<?php 
				$id = get_the_id();
				$terms = get_terms($taxName);
				foreach ($terms as $term) {
				$catTerm = $term->name;
				$catTerm = str_replace(' ','-', $catTerm);
				$catTerm = $catTerm.' ';
				echo '<button class="button-style button-8" type="button" data-filter=".'.$catTerm.'">'.$term->name.'</button>';
				}
				?>
			</div>
			<div class="search-holder border-button col-12 col-md-8 col-lg-4">
				<input placeholder="Search" class="search-news quicksearch filter-button" type="text" name="search-news">
				<span class="search-icon ion-android-search"></span>
			</div>

			<!-- SELECT DROP DOWN -->
			<!-- <div class="dropdown-filter col s12">
				<select class="filters-select border-button dropdown filter-button">
					<option selected="true" value="*">Show All</option>
					<?php $cats = get_terms([
					'taxonomy' => $taxName,
					'hide_empty' => 'true'

					]);
					foreach($cats as $cat){
					$name = $cat->name;
					$slug = $cat->slug;
					echo "<option value=.$slug>$name</option>";
					}
					wp_reset_postdata();
				?>
				</select>
			</div> -->
		</div>
		</div>
		<!-- NEWS HOLDER -->
		<div class="news-holder">
			<?php
			// The Loop
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$thumbnail = get_the_post_thumbnail_url();
				if($thumbnail == ""){
					$thumbnail = get_template_directory_uri().'/img/team-image-placeholder.jpg';
				}
				$title = get_the_title();
				$link = get_the_permalink();
				$terms = get_the_terms($post->ID, $taxName);
				$catTerm = '';
				$catList = '';
				$id = get_the_id();
				$date = get_the_date();
				$termSize = sizeof($terms);
				if($terms != ""){
					$counter = 1;
					foreach ($terms as $term) {
						$cat = $term->name;
						$cat = str_replace(' ','-', $cat);
						$cat = $cat.' ';
						if($counter == $termSize){
							$catString = $cat.'';
						}else{
							$catString = $cat.',';
						}
						
						$catTerm .= $cat;
						$catList .= $catString;
						$counter ++;
					}
				}
				$content = types_render_field('news-excerpt');
				if($content == ""){
					$content = get_the_content();
                    $content = strip_tags($content);
                    $content =  preg_replace('/\[.*?\]|/', '', $content);
                    $content = substr($content,0,300);
				}
				?>
				<div class="newsFeature threeCol <?php echo $catTerm?>" id="<?php echo $id ?>">
					<a href="<?php echo $link; ?>"><div class="thumbnail" style="background-image:url(<?php echo $thumbnail ?>)">
						<div class="cat-list"><p ><?php echo $catList ?></p></div>
					</div>
					<h3 class="title"><?php echo $title ?></h3>
					<div class="row">
					<div class="col s12">
						<div class="posted">
							<p class="lead">Post by <?php echo get_author_name(); ?>
							<span>|</span>
									<?php echo date('F, Y') ?></p>
						</div></div>
					</div>
					<p><?php echo $content; ?></p>
					<div>
						<a class="button-style button-9" href="<?php echo $link ?>">Read More</a>
					</div>
				</a>
				</div>	
			<?php	
			endwhile;
			endif;
			// Reset Post Data
			wp_reset_postdata();
			?>
		</div>
		<div class="center-align loadMoreNews"><a href="#" class="button-8 button-style load">Load More</a></div>
	</div>


