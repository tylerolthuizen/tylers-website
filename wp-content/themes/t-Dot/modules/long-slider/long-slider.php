<div class="container longSliderContainer">
       	 <div class="arrow-sliders right">
                 <button disabled class="prev disabled"><span class="ion-ios-arrow-back"></span></button>
                 <button class="next"><span class="ion-ios-arrow-forward"></span></button>
          </div>
           <div class="spacer" style="clear: both;"></div>
        <ul class="partners-slider slides">
               <?php 
          // The Loop
          $counter = 1; 
          $child = 0;
          if ( $the_query->have_posts() ) :
          while ( $the_query->have_posts() ) : $the_query->the_post();
            if($counter == 1){
              $first = 'first';
            }else{
              $first = '';
            }
            $count = $the_query->post_count;
           
            if($counter == 1){
                $first = "first ";
                      
            } else if ($counter == $count) {
                $last = "last";
            }else {
                $first ="";

            }
            $content = get_the_excerpt().' <a href="'.$link.'">Read More</a>';
            $title = get_the_title();
            $link = get_the_permalink();
          ?>
            <li class="partner slide <?php echo $first . $last ?>" data-number="<?php echo $child ?>">
                  <h3><?php echo $title ?></h3>
                  <p><?php echo $content ?></p>
            </li>

          <?php $child ++;  $counter ++;  

            endwhile;
            endif;
            // Reset Post Data
            wp_reset_postdata();
           ?>
           
         </ul> 
       </div>