

<!-- FILTER CONTROLS  -->

<div class="controls gallery-controls">
	<button class="button-style button-8 active" type="button" data-filter="*">All</button>
	<?php 
		$terms = get_terms($taxName);
		foreach ($terms as $term) {
				$catTerm = $term->name;
				$catTerm = str_replace(' ','-', $catTerm);
				$catTerm = $catTerm.' ';
		echo '<button class="button-style button-8" type="button" data-filter=".'.$catTerm.'">'.$term->name.'</button>';
		}

	 ?>
</div>
<!-- GALLERY AND WP QUERY LOOP -->
<div class="gallery-holder">
	<?php 
		// The Loop
		if ( $the_query->have_posts() ) :?>
		<div class="picture">
	<?php
		while ( $the_query->have_posts() ) : $the_query->the_post(); 
		$child_posts = types_child_posts('single-gallery');
		// print_r($child_posts);
		foreach  ( $child_posts as $child_post ) {
		$imageURL = $child_post->fields['gimage'];
		$imageId = attachment_url_to_postid($imageURL );
		$imageSize = wp_get_attachment_image_src($imageId , 'full');
		$image =  wp_get_attachment_image_src($imageId , $galleryCrop);
		$counter = 0;
		$terms = get_the_terms($child_post->ID, $taxName);
		$catTerm = '';
		$id = get_the_id();
		$index = 1;
		if($terms != ""){
			foreach ($terms as $term) {
				$cat = $term->name;
				$cat = str_replace(' ','-', $cat);
				$cat = $cat.' ';
				$catTerm .= $cat;
			}
		}
	?>
		<figure class="pic <?php echo $catTerm?>">
			<a class="no-barba" href="<?php echo $imageURL; ?>" data-size="<?php echo $imageSize[1] .'x'. $imageSize[2] ?>" data-index="<?php echo $counter; ?>">
				<img src="<?php echo $imageURL ?>" alt="gallery-image-<?php echo $counter ?>" width="<?php echo $image[1] ?>" height="<?php echo $image[2] ?>">
			</a>
		</figure>
	<?php
		$counter ++;
		}
		endwhile;
		endif;
		?></div>
		<?php
		// Reset Post Data
		wp_reset_postdata();

	 ?>


	 <!-- PHOTOSWIPE SET UP NEED CODE**** -->
	 <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="pswp__bg"></div>
	    <div class="pswp__scroll-wrap">
	 
	        <div class="pswp__container">
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	        </div>
	        <div class="pswp__ui pswp__ui--hidden">
	            <div class="pswp__top-bar">
	                <div class="pswp__counter"></div>
	                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
	                <button class="pswp__button pswp__button--share" title="Share"></button>
	                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
	                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
	                <div class="pswp__preloader">
	                    <div class="pswp__preloader__icn">
	                      <div class="pswp__preloader__cut">
	                        <div class="pswp__preloader__donut"></div>
	                      </div>
	                    </div>
	                </div>
	            </div>
	            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
	                <div class="pswp__share-tooltip"></div> 
	            </div>
	            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
	            </button>
	            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
	            </button>
	            <div class="pswp__caption">
	                <div class="pswp__caption__center"></div>
	            </div>
	        </div>
	    </div>
	</div>
</div>