<div class="team-holder">
	
	<?php
		$counter  = 1;
		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();
			
			$firstName = types_render_field('first-name');
			// $lastName = types_render_field('last-name');
			// $jobTitle = types_render_field('job-title');
			// $bio = types_render_field('bio');
			$featureImage = get_the_post_thumbnail_url();
			if($featureImage == ""){
				$featureImage = get_template_directory_uri().'/img/team-image-placeholder.jpg';
			}
			$id = get_the_id();
	?>
	<div class="team-feature" id="<?php echo $id ?>" data-position="<?php echo $counter ?>">
		<div class="content" style="background-image:url(<?php echo $featureImage ?>)">
			<div class="firstname-caption">
				<h3><?php echo $firstName ?></h3>
			</div>
		</div>
	</div>
	<?php	
		
		$counter ++;
		
			
		endwhile;
		endif;

	 ?>
	
</div>