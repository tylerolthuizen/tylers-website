<h2 class="titleMain"><?php echo $titleMain; ?></h2>	
<div><div class="arrows arrows-scroller2">
			<button data-direction="prev"><span class="ion-ios-arrow-back"></span></button>
			<button data-direction="next"><span class="ion-ios-arrow-forward"></span></button>
		</div></div>
		
<div class="outerSlideFrame">

	<div class="left-slider">
		
		<?php  
			// The Loop
			$counter = 1;
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
			$title = get_the_title();
			$excerpt = get_the_excerpt();
			$link = get_the_permalink();
			if($counter == 1){
				$active = 'active';
			}else{
				$active ="";
			}
			echo '
				<div class="slider-content '.$active.'" data-count="'.$counter.'">
					<h3> '.$title.' </h3>
					<p> '.$excerpt.' </p>
					<a class="button-style button-8" href="'.$link.'">View Project</a> 
				</div>
			';
			$counter ++;
			endwhile;
			endif;
			// Reset Post Data
			wp_reset_postdata();	
		?>
	</div>
	<div class="right-slider">
		<?php  
			// The Loop
			$counter = 1;
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
			
			$image = get_the_post_thumbnail_url();
			if($image == ""){
				$image = get_template_directory_uri().'/img/team-image-placeholder.jpg';
			}
			if($counter == 1){
				$active = 'active';
			}else{
				$active ="";
			}
			echo '
				<div class="image '.$active.'" data-count="'.$counter.'" style="background-image:url('.$image.');">
					
				</div>
			';
			$counter ++;
			endwhile;
			endif;
			// Reset Post Data
			wp_reset_postdata();	
		?>
	</div>
	</div>
